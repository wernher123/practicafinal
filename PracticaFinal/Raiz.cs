﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaFinal
{
    
    public class Raiz
    {
        public double CalcularRaiz(double numero1)
        {
            return Math.Sqrt(numero1);
        }
    }
}
